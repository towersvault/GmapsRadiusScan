import os
from flask import Flask, Response, render_template, request, redirect, session
from RadiusUtils import GMapsUtils, parse_html
from gmplot import gmplot


INIT_COORDINATES = (-33.903630, 18.420469)  # Example, these can be changed.

app = Flask(__name__)
gmaps = GMapsUtils(api_key='REDACTED, USE YOUR OWN GOOGLE MAPS API KEY',
        base_lat=INIT_COORDINATES[0], 
        base_lon=INIT_COORDINATES[1], 
        base_radius=700)


@app.route('/', methods=['GET'])
def home():
    return '''
        This still has to be completed.
        Go <a href="/map">here</a> in the meanwhile.
    '''


@app.route('/map/', methods=['GET', 'POST'])
def maps():
    # The code below has to be cleaned up.
    places = []
    places += gmaps.find_nearby('church')
    places += gmaps.find_nearby('business')
    # places += gmaps.find_nearby('restaurant', increased_radius=40)

    circle_center = gmaps.get_mean_vector(places)

    map_builder = gmplot.GoogleMapPlotter(circle_center[0], circle_center[1], 13)
    map_builder.circle(circle_center[0], 
            circle_center[1], 
            gmaps.base_radius,
            color='#00BFFF',
            alpha=0.2)
    map_builder.marker(circle_center[0], circle_center[1], title='Center of Scan')
    for p in places:
        map_builder.marker(p[1], p[2], color='#FFFFFF', title=p[0])
    
    # Building the HTML file.
    html_file_dir = os.path.join(app.root_path, 'templates', '')
    map_builder.draw(html_file_dir + 'temp_map.html')
    parse_html(html_file_dir)  # Fix the HTML file with proper URLs for assets.

    return render_template('map.html')


if __name__ == '__main__':
    app.run()
