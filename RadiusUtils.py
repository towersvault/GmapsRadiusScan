import googlemaps
from math import sin, cos, sqrt, atan2, radians


def parse_html(filename):
    """
    Because gmplot forces the marker images url to
    be something that Flask doesn't serve, and
    honestly would be a pain in the ass to work
    around in any other way. This is just less of
    a pain in the ass.
    """
    fin = open(filename + 'temp_map.html', 'r')
    lines = fin.read().splitlines()
    fin.close()
    for i in range(0, len(lines)):
        if lines[i].__contains__('var img = new google.maps.MarkerImage'):
            img_name = lines[i].replace(');', '')
            img_name = img_name.split('/')[-1].replace('\'', '')
            lines[i] = 'var img = new google.maps.MarkerImage(\'/static/markers/' + str(img_name) + '\');'
    fout = open(filename + 'map.html', 'w')
    for line in lines:
        fout.write(line)
        fout.write('\n')
    fout.close()


class GMapsUtils:

    def __init__(self, api_key, base_lat, base_lon, base_radius=500):
        """
        api_key: Google Maps API key.
        base_lat: The general latitude of the area you're trying to scan.
        base_lon: The general longitude of the area you're trying to scan.
        base_radius: The radius of the scan in meters. The rendered circle
            will use this value.
        """
        self.base_lat = base_lat
        self.base_lon = base_lon
        self.base_radius = base_radius
        self.__gmaps = googlemaps.Client(api_key)

    
    def calc_distance(self, vector1, vector2):
        """
        Calculates the distance in meters between two coordinate vector points.
        """
        EARTH_RADIUS = 6373.0

        lat1 = radians(vector1[0])
        lon1 = radians(vector1[1])
        lat2 = radians(vector2[0])
        lon2 = radians(vector2[1])
        dlat = lat2 - lat1
        dlon = lon2 - lon1
        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))
        distance = EARTH_RADIUS * c * 1000

        return round(distance, 2)


    def find_nearby(self, keyword, increased_radius=20):
        """
        keyword: A Google Maps Places keyword, such as church, school, business, etc.
            Google has a list here https://developers.google.com/places/supported_types
        increased_radius: % value. The base_radius plus base_radius * % gets scanned
            to improve accuracy when you try to find the mean vector.
        """
        geocode_results = self.__geocode_radar_search(keyword, 
                self.base_radius + self.base_radius * (increased_radius / 100.0))
        vectors = []
        for r in geocode_results:
            vectors.append((r['name'], r['lat'], r['lon']))

        return vectors


    def get_mean_vector(self, vectors):
        """
        vectors: Pass vectors provided by find_nearby()
        """
        mean_lat = 0
        mean_lon = 0
        for v in vectors:
            mean_lat += v[1]
            mean_lon += v[2]
        mean_lat = (1 / len(vectors)) * mean_lat
        mean_lon = (1 / len(vectors)) * mean_lon

        return (mean_lat, mean_lon)


    def __geocode_radar_search(self, keyword, radius):
        """
        keyword: A Google Maps Places keyword, such as church, school, business, etc.
            Google has a list here https://developers.google.com/places/supported_types
        radius: Scan radius in meters.

        Private function that pulls all the geocode data and parses it a bit from a 
        Google Maps Places Radar call.
        """
        results = self.__gmaps.places_radar(location=(self.base_lat, self.base_lon),
                radius=radius, keyword=keyword, type='basic')
        nearby_data = []
        for r in results['results']:
            place_id = r['place_id']
            geocode_data = self.__gmaps.place(place_id=place_id)['result']
            nearby_data.append({'name': geocode_data['name'],
                                'lat': geocode_data['geometry']['location']['lat'],
                                'lon': geocode_data['geometry']['location']['lng']})

        return nearby_data
